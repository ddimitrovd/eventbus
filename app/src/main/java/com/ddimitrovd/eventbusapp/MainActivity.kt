package com.ddimitrovd.eventbusapp

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ddimitrovd.eventbusapp.event.AppEventType
import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventBus
import com.ddimitrovd.eventbuslib.Listener
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.TextView
import com.ddimitrovd.eventbusapp.event.RunnerEvent




class MainActivity : AppCompatActivity(), Listener {

    private var totalAddedRunners = 0
    private var runnersAlive = 0
    private var runnersDied= 0
    private var runnersEscaped= 0
    var scrollLinearLayout: LinearLayout? = null


    override fun onEventReceived(event: Event) {
        runOnUiThread {
            if (event.getEventType() == AppEventType.RUNNER_ESCAPED_EVENT && event is RunnerEvent) {
                runnersAlive--
                runnersEscaped++
                showEventDescriptionOnView(ESCAPED_STORIES.random()+event.runnerNumber, Color.GREEN)
                updateView()
            } else if (event.getEventType() == AppEventType.RUNNER_DIED_EVENT && event is RunnerEvent) {
                runnersAlive--
                runnersDied++
                showEventDescriptionOnView(DIED_STORIES.random()+event.runnerNumber+"/"+event.position, Color.RED)
                updateView()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        scrollLinearLayout = scroll_liner_layout
        updateView()

        subscribeForEvents()
        Thread(Monster()).start()

        add_button.setOnClickListener { addRunner() }
    }

    fun subscribeForEvents() {
        EventBus.subscribeToEvent(AppEventType.RUNNER_ESCAPED_EVENT, this)
        EventBus.subscribeToEvent(AppEventType.RUNNER_DIED_EVENT, this)
        EventBus.subscribeToEvent(AppEventType.MONSTER_APPEARED_EVENT, this)
    }

    private fun addRunner() {
        Thread(Runner(totalAddedRunners++)).start()
        runnersAlive++
        updateView()
    }

    @SuppressLint("SetTextI18n")
    fun updateView() {
        total_text_view.text = getString(R.string.total_added_runners_text) + " " + totalAddedRunners
        avile_text_view.text = getString(R.string.alive_runners_text) + " " + runnersAlive
        escaped_text_view.text = getString(R.string.escaped_runners_text) + " " + runnersEscaped
        died_text_view.text = getString(R.string.dead_runners_text) + " " + runnersDied
    }

    fun showEventDescriptionOnView(message: String, color: Int) {
        val tv = TextView(this)
        tv.text = message
        tv.setTextColor(color)
        tv.setPadding(10,20,10,0)
        scrollLinearLayout!!.addView(tv,0)
    }
}
