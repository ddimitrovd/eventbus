package com.ddimitrovd.eventbusapp

import com.ddimitrovd.eventbusapp.event.AppEventType
import com.ddimitrovd.eventbusapp.event.MonsterAppearedEvent
import com.ddimitrovd.eventbuslib.EventBus

class Monster: Runnable {

    private var myPosition = 0

    override fun run() {
        // Monster never rests!
        while(true) {
            myPosition = (0 until LINE_LENGTH).random()
            appear()
            Thread.sleep(MONSTER_SLEEP)
        }
    }

    private fun appear() {
        EventBus.acceptEvent(MonsterAppearedEvent(AppEventType.MONSTER_APPEARED_EVENT, myPosition))
    }

}