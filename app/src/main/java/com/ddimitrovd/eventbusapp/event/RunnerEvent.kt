package com.ddimitrovd.eventbusapp.event

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventType

class RunnerEvent(private val eventType: AppEventType, val runnerNumber: Int, val position: Int): Event {

    override fun getEventType(): EventType {
        return eventType
    }
}