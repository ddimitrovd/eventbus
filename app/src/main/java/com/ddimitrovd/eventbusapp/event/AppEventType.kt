package com.ddimitrovd.eventbusapp.event

import com.ddimitrovd.eventbuslib.EventType

enum class AppEventType: EventType {
    MONSTER_APPEARED_EVENT,
    RUNNER_ESCAPED_EVENT,
    RUNNER_DIED_EVENT;
}