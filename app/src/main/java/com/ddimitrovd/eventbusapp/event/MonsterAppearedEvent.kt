package com.ddimitrovd.eventbusapp.event

import com.ddimitrovd.eventbusapp.event.AppEventType
import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventType

class MonsterAppearedEvent(private val eventType: AppEventType, val position: Int): Event {

    override fun getEventType(): EventType {
        return eventType
    }
}