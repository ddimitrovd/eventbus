package com.ddimitrovd.eventbusapp

import com.ddimitrovd.eventbusapp.event.AppEventType
import com.ddimitrovd.eventbusapp.event.MonsterAppearedEvent
import com.ddimitrovd.eventbusapp.event.RunnerEvent
import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventBus
import com.ddimitrovd.eventbuslib.Listener

class Runner(val number: Int): Runnable, Listener {

    private var encounteredMonster = false
    private var escaped = false
    private var myPosition = 0

    override fun onEventReceived(event: Event) {
        if (event.getEventType() == AppEventType.MONSTER_APPEARED_EVENT && event is MonsterAppearedEvent) {
            if ((event as MonsterAppearedEvent).position == myPosition)
                encounteredMonster = true
        }
    }

    override fun run() {
        EventBus.subscribeToEvent(AppEventType.MONSTER_APPEARED_EVENT, this)

        while (!encounteredMonster && !escaped) {
            myPosition++
            Thread.sleep(RUNNER_SLEEP)
            if (myPosition >= LINE_LENGTH) escape()
        }

        if (encounteredMonster) commitSeppuku()
        return
    }

    private fun commitSeppuku() {
        EventBus.acceptEvent(RunnerEvent(AppEventType.RUNNER_DIED_EVENT, number, myPosition))
    }

    private fun escape() {
        EventBus.acceptEvent(RunnerEvent(AppEventType.RUNNER_ESCAPED_EVENT, number, myPosition))
        escaped = true
    }
}