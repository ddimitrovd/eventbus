package com.ddimitrovd.eventbusapp

const val LINE_LENGTH = 100
const val MONSTER_SLEEP = 100L
const val RUNNER_SLEEP = 300L

val ESCAPED_STORIES = setOf("Such a relief! He is free of this nightmare. - runner: ",
    "Praise the brave hero! He has conquered. - runner: ",
    "Escaped he has! - runner: ")
val DIED_STORIES = setOf("Oh what horror! He was petrified on place in a rather embarrassing posture. - runner/position: ",
    "It is over! The monster has the high ground. - runner/position: ",
    "Such a pity! So young... So dead. - runner/position: ",
    "Maybe he is better of dead than in this horrible game. - runner/position: ")