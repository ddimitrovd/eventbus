package com.ddimitrovd.eventbuslib


/**
 * This interface is to be implemented by Event classes that carry different data according to what is required
 * by the listeners to process. It is recommended that before using an Event derived class it's actual class be
 * checked as data types and methods may differ.
 *
 * Whether multiple Event derived classes have the same EventType or multiple instances of the same Event have different
 * EventType is up to the user's implementation.
 *
 * It is important to notice that Event dispatching and subscriptions by listeners is based on the EventType and not on
 * the Event subclass
 *
 *
 *
 * !! UPDATE: Now that I have used my library in the second task (Video Player) I have doubts about this way of
 * implementation. Event tho is maybe allows for more freedom of implementation, it becomes confusing in a larger project
 * and in a project with multiple devs it might pose some serious problems. It is too late to change it now, but I thing
 * that if I had to start all over again I would base the events solely on their class, not the enum Type.
 */
interface Event {

    fun getEventType(): EventType

}