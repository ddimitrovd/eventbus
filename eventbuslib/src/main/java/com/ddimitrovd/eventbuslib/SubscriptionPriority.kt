package com.ddimitrovd.eventbuslib

enum class SubscriptionPriority {
    HIGH,
    DEFAULT,
    LOW;
}