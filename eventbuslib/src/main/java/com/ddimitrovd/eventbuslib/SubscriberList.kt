package com.ddimitrovd.eventbuslib

/**
 * This class is for internal lib use only and represents a list of the listeners with different priorities
 */
internal class SubscriberList {

    private var highSet = mutableSetOf<Listener>()
    private var defaultSet = mutableSetOf<Listener>()
    private var lowSet = mutableSetOf<Listener>()

    private fun contains(listener: Listener): Boolean {
        return highSet.contains(listener) || defaultSet.contains(listener) || lowSet.contains(listener)
    }

    fun add(listener: Listener, priority: SubscriptionPriority) {
        // Check for duplication
        if (contains(listener)) return

        // Add to appropriate set
        when (priority) {
            SubscriptionPriority.HIGH -> highSet.add(listener)
            SubscriptionPriority.DEFAULT -> defaultSet.add(listener)
            else -> lowSet.add(listener)
        }
    }

    fun remove(listener: Listener) {
        if(!highSet.remove(listener))
            if(!defaultSet.remove(listener))
                lowSet.remove(listener)
    }

    fun getPrioritizedSet(): Set<Listener>{
        return highSet.union(defaultSet.union(lowSet))
    }

}