package com.ddimitrovd.eventbuslib

/**
 * Listener interface to be implemented by classes that want to listen for events on the EventBus
 */
interface Listener {

    /**
     * This function gets called by the EventBus on a matching subscription Event. Any further processing should
     * ideally be made in another thread
     */
    fun onEventReceived (event: Event)

}