package com.ddimitrovd.eventbuslib


/**
 * The EventBus manages the subscriptions to events and dispatches accepted Events to it's listeners
 */
object EventBus {

    private var eventsListeners: HashMap<EventType, SubscriberList> = HashMap()

    /**
     * Subscribe listener to an event type. One listener class can be subscribed to multiple event types, but only once
     * for the same event type.
     *
     * @param eventType The EventType subscribing for
     * @param listener The Listener instance that is subscribing for the EventType
     * @param priority The SubscriptionPriority of the listener for this EventType (optional)
     */
    @Synchronized fun subscribeToEvent(eventType: EventType, listener: Listener, priority: SubscriptionPriority = SubscriptionPriority.DEFAULT) {
        if (!eventsListeners.containsKey(eventType)) {
            // Event type does not exists in list so create it
            eventsListeners[eventType] = SubscriberList()
        }
        eventsListeners.getValue(eventType).add(listener, priority)
    }

    /**
     * Unsubscribe listener form an EventType
     */
    @Synchronized fun unsubscribeFormEvent(eventType: EventType, listener: Listener) {
        if (eventsListeners.containsKey(eventType)) {
            // Remove subscriber from event type list if subscriber is present in corresponding set
            eventsListeners.getValue(eventType).remove(listener)
        }
    }

    /**
     * Accept an Event subclass object and dispatch to all listeners for it's Event.EventType
     *
     * TODO: Now here is where I would go talking to my superior.
     * My dilemma is whether for each subscriber a new thread should be created calling onEventReceived?
     *    - This would be nice if the user went ahead and started doing some heavy processing without
     *      himself creating a new thread it would not prevent the rest of the subscribers being notified.
     *
     *    - I have not implemented it this way tho, because I think a low level library as this one should leave the
     *      user the freedom to implement this as he wishes (Although, it is now easy for the user to break the desired
     *      functioning of the library). Also the way it is implemented now assures that after the end of
     *      acceptEvent , all the subscribers have received the event. (Conveniently it is also easier to test ;p)
     */
    @Synchronized fun acceptEvent(event: Event) {
        if (eventsListeners.containsKey(event.getEventType())) {
            // Broadcast to all the listeners in set
            for (sub in eventsListeners.getValue(event.getEventType()).getPrioritizedSet()) {
                sub.onEventReceived(event)
            }
        }
    }
}