package com.ddimitrovd.eventbuslib


/**
 * This interface should be implemented in an Enum by the library user to describe all the Event types he wishes to create
 * @sample
 *  enum class FakeEventType: EventType {
        FAKE_EVENT_A,
        FAKE_EVENT_B;

        override fun eventName(): String {
            return this.name
        }
    }
 *
 * !!! It is important no two event have the same name, as events will get mixed up!
 */
interface EventType