package com.ddimitrovd.eventbuslib

import com.ddimitrovd.eventbuslib.fakes.FakeEventA
import com.ddimitrovd.eventbuslib.fakes.FakeEventB
import com.ddimitrovd.eventbuslib.fakes.FakeEventType
import com.ddimitrovd.eventbuslib.fakes.FakeListener
import org.junit.Assert.assertEquals
import org.junit.Test

class EventBusSubscribingUnitTest {

    @Test
    fun singleSubscriberToOneEventTest() {

        // create listener
        val listener = FakeListener()

        // subscribe to Event A
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listener)

        // Fire event
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"))

        // Exactly one Event was received and data is same
        assertEquals(1,listener.eventsReceived.size)
        assertEquals("ImportantString",(listener.eventsReceived[0] as FakeEventA).getSomeImportantData())
    }

    @Test
    fun singleSubscriberToMultipleEventsTest() {

        // create listener
        val listener = FakeListener()

        // subscribe to Event A and Event B
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listener)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_B, listener)

        // Fire events
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"))
        EventBus.acceptEvent(FakeEventB(FakeEventType.FAKE_EVENT_B, 13.6f))

        // Exactly two events were received
        assertEquals(2,listener.eventsReceived.size)
    }

    @Test
    fun multipleSubscribersToSingleEventTest() {

        // create listeners
        val listenerOne = FakeListener()
        val listenerTwo = FakeListener()

        // subscribe to Event A
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerOne)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerTwo)

        // Fire event
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"))

        // Each Listener received one event
        assertEquals(1,listenerOne.eventsReceived.size)
        assertEquals(1,listenerTwo.eventsReceived.size)
    }

    @Test
    fun multipleSubscribersToMultipleEventsTest() {

        // create listeners
        val listenerOne = FakeListener()
        val listenerTwo = FakeListener()

        // subscribe two listeners to Event A and one to Event B
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerOne)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerTwo)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_B, listenerTwo)

        // Fire event
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"))
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_B, "ImportantString"))

        // Each Listener received the appropriate amount of events
        assertEquals(1,listenerOne.eventsReceived.size)
        assertEquals(2,listenerTwo.eventsReceived.size)
    }


    @Test
    fun onlyOneSubscriptionForAnEventTypeByListenerTest() {

        // create listener
        val listener = FakeListener()

        // subscribe to Event A several times
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listener)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listener)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listener)

        // Fire event
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"))

        // Exactly one Event was received and data is same
        assertEquals(1,listener.eventsReceived.size)
        assertEquals("ImportantString",(listener.eventsReceived[0] as FakeEventA).getSomeImportantData())
    }
}
