package com.ddimitrovd.eventbuslib.fakes

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.Listener


// I couldn't for the life of me import Mockito for some reason. So a Fake it is.
class FakeListener : Listener {

    val eventsReceived  = ArrayList<Event>()

    override fun onEventReceived(event: Event) {
        eventsReceived.add(event)
    }
}