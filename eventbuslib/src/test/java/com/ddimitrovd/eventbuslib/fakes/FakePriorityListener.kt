package com.ddimitrovd.eventbuslib.fakes

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.Listener

class FakePriorityListener : Listener {

    val eventsReceived  = ArrayList<Event>()
    val eventsReceivedTime  = ArrayList<Long>()

    override fun onEventReceived(event: Event) {
        eventsReceived.add(event)
        eventsReceivedTime.add(System.currentTimeMillis())
        Thread.sleep(5)
    }
}