package com.ddimitrovd.eventbuslib.fakes

import com.ddimitrovd.eventbuslib.EventType

enum class FakeEventType: EventType {
    FAKE_EVENT_A,
    FAKE_EVENT_B;
}