package com.ddimitrovd.eventbuslib.fakes

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventType

class FakeEventA(private val eventType: FakeEventType, private val someImportantData: String) : Event {

    override fun getEventType(): EventType {
        return eventType
    }

    fun getSomeImportantData(): String {
        return someImportantData
    }
}