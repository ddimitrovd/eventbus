package com.ddimitrovd.eventbuslib.fakes

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventType

class FakeEventB(private val eventType: FakeEventType, private val someUselessData: Float) : Event {

    override fun getEventType(): EventType {
        return eventType
    }

    fun someUselessData(): Float {
        return someUselessData
    }
}