package com.ddimitrovd.eventbuslib

import com.ddimitrovd.eventbuslib.fakes.FakeEventA
import com.ddimitrovd.eventbuslib.fakes.FakeEventB
import com.ddimitrovd.eventbuslib.fakes.FakeEventType
import com.ddimitrovd.eventbuslib.fakes.FakeListener
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread

class EventBusMultithreadingUnitTest {

    private fun fireEventMultipleTimes(event: Event, t: Int){
        for (i in 1..t ){
            EventBus.acceptEvent(event)
            Thread.sleep(10)
        }
    }

    @Test
    fun multipleListenersAndMultipleEventFiringThreadsTest() {

        // create listeners
        val listenerOne = FakeListener()
        val listenerTwo = FakeListener()
        val listenerThree = FakeListener()

        // Subscribe One to A, Two To B and Three to both
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerOne)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_B, listenerTwo)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerThree)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_B, listenerThree)

        // Events of type to be submitted by each thread
        val eventsByThreadsTypeA = 60
        val numberOfThreadsOfTypeA = 20
        val eventsByThreadsTypeB = 80
        val numberOfThreadsOfTypeB = 15


        val executor = Executors.newFixedThreadPool(500)
        // Add 10 event creating threads for Event A
        for (i in 1..numberOfThreadsOfTypeA) {
            val worker = thread(start = false) {
                fireEventMultipleTimes(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"),eventsByThreadsTypeA)
            }
            executor.execute(worker)
        }
        // Add 10 event creating threads for Event B
        for (i in 1..numberOfThreadsOfTypeB) {
            val worker = thread(start = false) {
                fireEventMultipleTimes(FakeEventB(FakeEventType.FAKE_EVENT_B, 16.5f),eventsByThreadsTypeB)
            }
            executor.execute(worker)
        }

        // Start execution of concurrent threads
        executor.shutdown()

        // Await termination of threads
        executor.awaitTermination(5, TimeUnit.SECONDS)

        // Assert number of events received for the listeners
        val totalEventsA = eventsByThreadsTypeA*numberOfThreadsOfTypeA
        val totalEventsB = eventsByThreadsTypeB*numberOfThreadsOfTypeB
        assertEquals(totalEventsA, listenerOne.eventsReceived.size)
        assertEquals(totalEventsB, listenerTwo.eventsReceived.size)
        assertEquals(totalEventsA+totalEventsB, listenerThree.eventsReceived.size)
    }

}