package com.ddimitrovd.eventbuslib

import com.ddimitrovd.eventbuslib.fakes.FakeEventA
import com.ddimitrovd.eventbuslib.fakes.FakeEventB
import com.ddimitrovd.eventbuslib.fakes.FakeEventType
import com.ddimitrovd.eventbuslib.fakes.FakeListener
import org.junit.Assert.assertEquals
import org.junit.Test

class EventBusUnsubscribingUnitTest {

    @Test
    fun unsubscribingFromSingleEventTest() {

        // create listener
        val listener = FakeListener()

        // subscribe to Event A
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listener)

        // Fire event
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"))

        // unsubscribe form event A
        EventBus.unsubscribeFormEvent(FakeEventType.FAKE_EVENT_A, listener)

        // Fire event again
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"))

        // Exactly one Event was received
        assertEquals(1, listener.eventsReceived.size)
    }

    @Test
    fun unsubscribingFromOneEventDoesNotAffectOtherSubscriptionsOfSameListenerTest() {
        // create listener
        val listener = FakeListener()

        // subscribe to Event A and B
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listener)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_B, listener)

        // unsubscribe form event A
        EventBus.unsubscribeFormEvent(FakeEventType.FAKE_EVENT_A, listener)

        // Fire event B
        EventBus.acceptEvent(FakeEventB(FakeEventType.FAKE_EVENT_B, 12f))

        // Exactly one Event was received
        assertEquals(1, listener.eventsReceived.size)
    }

    @Test
    fun unsubscribingMultipleListenersFromSameEvent() {

        // create listeners
        val listenerOne = FakeListener()
        val listenerTwo = FakeListener()
        val listenerThree = FakeListener()

        // subscribe listeners to Event A
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerOne)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerTwo)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerThree)

        // unsubscribe two listeners
        EventBus.unsubscribeFormEvent(FakeEventType.FAKE_EVENT_A, listenerOne)
        EventBus.unsubscribeFormEvent(FakeEventType.FAKE_EVENT_A, listenerThree)

        // Fire event
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"))

        // Listener two still receives
        assertEquals(1,listenerTwo.eventsReceived.size)
    }

    @Test
    fun unsubscribingWithNoSubscription() {

        // create listener
        val listener = FakeListener()

        // unsubscribe form event
        EventBus.unsubscribeFormEvent(FakeEventType.FAKE_EVENT_A, listener)

        // It should not crash that's it
    }
}