package com.ddimitrovd.eventbuslib

import com.ddimitrovd.eventbuslib.fakes.FakeEventA
import com.ddimitrovd.eventbuslib.fakes.FakeEventType
import com.ddimitrovd.eventbuslib.fakes.FakePriorityListener
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class EventBusPriorityUnitTest {

    @Test
    fun multipleSubscribersToSingleEventTest() {

        // create listeners
        val listenerOne = FakePriorityListener()
        val listenerTwo = FakePriorityListener()
        val listenerThree = FakePriorityListener()
        val listenerFour = FakePriorityListener()
        val listenerFive = FakePriorityListener()

        // subscribe to Event A with respective priorities
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerOne)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerTwo)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerThree, SubscriptionPriority.HIGH)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerFour, SubscriptionPriority.LOW)
        EventBus.subscribeToEvent(FakeEventType.FAKE_EVENT_A, listenerFive)

        // Fire event
        EventBus.acceptEvent(FakeEventA(FakeEventType.FAKE_EVENT_A, "ImportantString"))

        // ListenerThree must be the first to receive, ListenerFour must be last and for the test to be significant those times should be different
        val listOfListeners = listOf(listenerOne,listenerTwo,listenerThree,listenerFour,listenerFive)
        assertNotEquals(listenerThree,listenerFour)
        assertEquals(listenerThree.eventsReceivedTime[0], listOfListeners.minBy{it.eventsReceivedTime[0]}!!.eventsReceivedTime[0])
        assertEquals(listenerFour.eventsReceivedTime[0], listOfListeners.maxBy{it.eventsReceivedTime[0]}!!.eventsReceivedTime[0])
    }
}